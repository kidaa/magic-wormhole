
User-visible changes in "magic-wormhole":

## Release 0.3.0 (24-Jun-2015)

Add preliminary Twisted support, only for symmetric endpoints (no
initator/receiver distinction). Lacks code-entry tab-completion. May still
leave timers lingering. Add test suite (only for Twisted, so far).

Use a sqlite database for Relay server state, to survive reboots with less
data loss. Add "--advertise-version=" to "wormhole relay start", to override
the version we recommend to clients.

## Release 0.2.0 (10-Apr-2015)

Initial release: supports blocking/synchronous asymmetric endpoints
(Initiator on one side, Receiver on the other). Codes can be generated by
Initiator, or created externally and passed into both (as long as they start
with digits: NNN-anything).
